<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = "buku";
    protected $fillable = ["judul", "pengarang", "penerbit", "tahun", "tumbnail", "kategori_buku_id"];

    public function buku(){
        return $this->hasMany('App\Pinjam_Buku');
    }
}
