<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profile";
    protected $fillable = ["firstname", "lastname", "gender", "address", "birthdate", "user_id"];
}
