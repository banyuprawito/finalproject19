<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Kategori_BukuController extends Controller
{
    public function create(){
        return view('kategori.create');
    }

    public function store(Request $request){
        $request->validate([
            'kategori' => 'required',
            'keterangan' => 'required', 
        ],

        [
            'Kategori.required' => "Nama tidak boleh kosong, silakan isi terlebih dahulu",
            'Keterangan.required' => "Umur tidak boleh kosong, silakan isi terlebih dahulu",
        ]
    
    );

    DB::table('kategori_buku')->insert(
        [
            'kategori' => $request['kategori'],
            'keterangan' => $request['keterangan'],
        ]
    );

    return redirect('/kategori');
    }

    public function index(){
        $kategori_buku = DB::table('kategori_buku')->get();
 
        return view('kategori.index', compact('kategori_buku'));
    }

    public function show($id){
        $kategori_buku = DB::table('kategori_buku')->where('id', $id)->first();
    
        return view('kategori.show', compact('kategori_buku'));
    }

    public function edit($id){
        $kategori_buku = DB::table('kategori_buku')->where('id', $id)->first();

        return view('kategori.edit', compact('kategori_buku'));
    }

    public function update($id, Request $request){
        $request->validate([
            'kategori' => 'required',
            'keterangan' => 'required', 
        ],

        [
            'kategori.required' => "Kategori tidak boleh kosong, silakan isi terlebih dahulu",
            'keterangan.required' => "Keterangan tidak boleh kosong, silakan isi terlebih dahulu",
        
        ]
    );
            DB::table('kategori_buku')->where('id', $id)->update(
                [
                    'kategori' => $request['kategori'],
                    'keterangan' => $request['keterangan']
                ]
            );
            return redirect('/kategori');
    }

    public function destroy($id){
        DB::table('kategori_buku')->where('id', '=', $id)->delete();

        return redirect('/kategori');
    }
}

