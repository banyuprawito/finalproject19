<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori_Buku;
use App\Buku;
use File;

class BukuController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();
        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori_buku = kategori_buku::all();

        return view('buku.create', compact('kategori_buku'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function store(Request $request){
            $request->validate([
                'judul' => 'required',
                'pengarang' => 'required',
                'penerbit' => 'required',
                'tahun' => 'required',
                'tumbnail' => 'required|mimes:jpg,jpeg,png|max:2200',
                'kategori_buku_id' => 'required', 
            ],
    
            [
                'judul.required' => "Judul tidak boleh kosong, silakan isi terlebih dahulu",
                'pengarang.required' => "Pengarang tidak boleh kosong, silakan isi terlebih dahulu",
                'penerbit.required' => "Penerbit tidak boleh kosong, silakan isi terlebih dahulu",
                'tahun.required' => "Tahun tidak boleh kosong, silakan isi terlebih dahulu",
                'tumbnail.required' => "Thumbnail tidak boleh kosong, silakan isi terlebih dahulu",
                'tumbnail.mimes' => "Thumbnail hanya bisa file dalam format jpg,jpeg,png dengan size maksimal 2MB, silakan isi terlebih dahulu",
                'kategori_buku_id.required' => "Kategori Buku tidak boleh kosong, silakan isi terlebih dahulu"

            ]
        
        );

        $gambar = $request->tumbnail;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();

        $buku = new Buku;
        $buku->judul = $request->judul;
        $buku->pengarang = $request->pengarang;
        $buku->penerbit = $request->penerbit;
        $buku->tahun = $request->tahun;
        $buku->kategori_buku_id = $request->kategori_buku_id;
        $buku->tumbnail = $new_gambar;
        $buku->save();

        $gambar->move('thumbnail/', $new_gambar);

        return redirect('/buku');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::findOrFail($id);
        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::findOrFail($id);
        $kategori_buku = kategori_buku::all();
        return view('buku.edit', compact('buku', 'kategori_buku'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'pengarang' => 'required',
            'penerbit' => 'required',
            'tahun' => 'required',
            'tumbnail' => 'mimes:jpg,jpeg,png|max:2200',
            'kategori_buku_id' => 'required', 
        ],

        [
            'judul.required' => "Judul tidak boleh kosong, silakan isi terlebih dahulu",
            'pengarang.required' => "Pengarang tidak boleh kosong, silakan isi terlebih dahulu",
            'penerbit.required' => "Penerbit tidak boleh kosong, silakan isi terlebih dahulu",
            'tahun.required' => "Tahun tidak boleh kosong, silakan isi terlebih dahulu",
            'tumbnail.mimes' => "Thumbnail hanya bisa file dalam format jpg,jpeg,png dengan size maksimal 2MB, silakan isi terlebih dahulu",
            'kategori_buku_id.required' => "Kategori Buku tidak boleh kosong, silakan isi terlebih dahulu"

        ]
    
    );

        $buku = Buku::find($id);

        if($request->has("tumbnail")){
            $path = "thumbnail/";
            File::delete($path . $buku->tumbnail);
            $gambar = $request->tumbnail;
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move('thumbnail/', $new_gambar);

            $buku->tumbnail = $new_gambar;
        }

        $buku->judul = $request->judul;
        $buku->pengarang = $request->pengarang;
        $buku->penerbit = $request->penerbit;
        $buku->tahun = $request->tahun;
        $buku->kategori_buku_id = $request->kategori_buku_id;
        
        $buku->save();

        return redirect('/buku');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::find($id);
        $path = "thumbnail/";
        FIle::delete($path.$buku->tumbnail);
        $buku->delete();

        return redirect('/buku');
    }
}
