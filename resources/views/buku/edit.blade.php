@extends('layout.master')
@section('judul')
    Halaman Edit Buku
@endsection

@section('content')
    <form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method("put")
        <div class="form-group">
        <label>Judul Buku</label>
        <input type="text" value="{{$buku->judul}}" class="form-control" name="judul">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
        <label>Pengarang Buku</label>
        <input type="text" value="{{$buku->pengarang}}" class="form-control" name="pengarang">
        </div>
        @error('pengarang')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
        <label>Penerbit</label>
        <input type="text" value="{{$buku->penerbit}}" class="form-control" name="penerbit">
        </div>
        @error('penerbit')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
        <label>Tahun Terbit</label>
        <input type="integer" value="{{$buku->tahun}}" class="form-control" name="tahun">
        </div>
        @error('tahun')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="exampleInputPassword1">Kategori Buku</label><br>
            <select name="kategori_buku_id" id="" class="form_control">
                <option value="">----Pilih Kategori----</option>
                @foreach ($kategori_buku as $item)
                @if ($item->id === $buku->kategori_buku_id)
                <option value="{{$item->id}}" selected>{{$item->kategori}}</option>
                @else
                <option value="{{$item->id}}">{{$item->kategori}}</option>
                @endif
                    
                @endforeach
            </select>
            </div>
            @error('kategori_buku_id')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
        <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" class="form-control" name="tumbnail">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection