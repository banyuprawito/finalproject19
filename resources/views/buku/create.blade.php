@extends('layout.master')
@section('judul')
    Halaman Data Buku
@endsection

@section('content')
    <form action="/buku" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
        <label>Judul Buku</label>
        <input type="text" class="form-control" name="judul">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
        <label>Pengarang Buku</label>
        <input type="text" class="form-control" name="pengarang">
        </div>
        @error('pengarang')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
        <label>Penerbit</label>
        <input type="text" class="form-control" name="penerbit">
        </div>
        @error('penerbit')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
        <label>Tahun Terbit</label>
        <input type="integer" class="form-control" name="tahun">
        </div>
        @error('tahun')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="exampleInputPassword1">Kategori Buku</label><br>
            <select name="kategori_buku_id" id="" class="form_control">
                <option value="">----Pilih Kategori----</option>
                @foreach ($kategori_buku as $item)
                    <option value="{{$item->id}}">{{$item->kategori}}</option>
                @endforeach
            </select>
            </div>
            @error('kategori_buku_id')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
        <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" class="form-control" name="tumbnail">
        </div>
        @error('tumbnail')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection