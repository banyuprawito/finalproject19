@extends('layout.master')
@section('judul')
    Halaman Daftar Buku
@endsection

@section('content')
<a href="/buku/create" class="btn btn-success my-2">Tambah Buku</a>
<div class="row">
    @forelse ($buku as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('thumbnail/'. $item->tumbnail)}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h2 class="card-text">{{$item->judul}}</h2>
                  <h5 class="card-text">{{$item->pengarang}}</h5>
                  <p class="card-text">{{$item->penerbit}}</p>
                  <p class="card-text">{{$item->tahun}}</p>
                  
                    
                <form action="/buku/{{$item->id}}" method="post">
                  @method("delete")
                  @csrf
                  <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/buku/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">  
                </form>
                </div>
              </div>

        </div>
    @empty

    <h4>Data Kosong</h4>
        
    @endforelse
</div>
@endsection