@extends('layout.master')
@section('judul')
    Halaman Detail Buku
@endsection

@section('content')
<div class="row">
        <div class="col-12">
            <div class="card">
                <img src="{{asset('thumbnail/'. $buku->tumbnail)}}" class="card-img-top" alt="...">
                  <h2 class="card-text">{{$buku->judul}}</h2>
                  <h5 class="card-text">{{$buku->pengarang}}</h5>
                  <p class="card-text">{{$buku->penerbit}}</p>
                  <p class="card-text">{{$buku->tahun}}</p>
                  <a href="/buku" class="btn btn-info btn-sm">Kembali</a>
                  <br>
                  <a href="/pinjam/create" class="btn btn-succes btn-sm">Pinjam Buku</a>
              </div>

        </div>
</div>
@endsection