@extends('layout.master')
@section('judul')
    Halaman Edit Profile
@endsection

@section('content')
    <form action="/profile/{{$profile->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>First Name</label>
            <input type="text" value="{{$profile->firstname}}" class="form-control" name="firstname">
            </div>
            @error('firstname')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
        <div class="form-group">
            <label>Last Name</label>
            <input type="text" value="{{$profile->lastname}}" class="form-control" name="lastname">
            </div>
                @error('lastname')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
        <div class="form-group">
        <label>Gender</label>
        <input type="text" value="{{$profile->gender}}" class="form-control" name="gender">
        </div>
        @error('gender')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label>Alamat</label>
            <input type="text" value="{{$profile->address}}" class="form-control" name="address">
            </div>
            @error('address')
                <div class="alert alert-danger">{{$message}}</div>
            @enderror
        <div class="form-group">
        <label>Birth</label>
        <input type="text" value="{{$profile->birthdate}}" class="form-control" name="birthdate">
        </div>
        @error('birthdate')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection