@extends('layout.master')
@section('judul')
    Halaman Tambah Kategori
@endsection

@section('content')
    <form action="/kategori" method="POST">
        @csrf
        <div class="form-group">
        <label>Nama Kategori</label>
        <input type="text" class="form-control" name="kategori">
        </div>
        @error('kategori')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
        <label>Keterangan</label>
        <input type="text" class="form-control" name="keterangan">
        </div>
        @error('bio')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection