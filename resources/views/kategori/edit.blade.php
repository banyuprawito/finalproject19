@extends('layout.master')
@section('judul')
    Halaman Edit Kategori Buku
@endsection

@section('content')
    <form action="/kategori/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
        <label>Nama Kategori</label>
        <input type="text" value="{{$cast->kategori}}" class="form-control" name="kategori">
        </div>
        @error('kategori')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
        <label>Keterangan</label>
        <input type="text" value="{{$kategori_buku->keterangan}}" class="form-control" name="keterangan">
        </div>
        @error('keterangan')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection