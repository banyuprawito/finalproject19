@extends('layout.master')
@section('judul')
    Halaman Daftar Kategori Buku
@endsection

@section('content')

<a href="/kategori/create" class="btn btn-primary my-3">Tambah Kategori Buku</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Kategori</th>
        <th scope="col">Keterangan</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($kategori_buku as $key => $item)
          <tr>
              <td>
                  {{$key+1}}
              </td>
              <td>
                  {{$item->kategori}}
              </td>
              <td>
                {{$item->keterangan}}
            </td>
            <td>
                
                <form action="/kategori/{{$item->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
          </tr>
      @empty
         <h1>Data tidak ditemukan</h1> 
      @endforelse
    </tbody>
  </table>
@endsection