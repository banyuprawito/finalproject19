<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    //CRUD kategori_buku

//mengarah ke form create data
Route::get('/kategori/create', "Kategori_BukuController@create");
//menyimpan data ke tabel cast
Route::post('/kategori', 'Kategori_BukuController@store');

//tampilkan semua data
Route::get('/kategori', 'Kategori_BukuController@index');
//detail data
Route::get('/kategori/{kategori_buku_id}', 'Kategori_BukuController@show');

//mengarah ke form edit data
Route::get('/kategori/{kategori_buku_id}/edit', 'Kategori_BukuController@edit');
//update data ke tabel cast
Route::put('/kategori/{kategori_buku_id}', 'Kategori_BukuController@update');

//dellete data
Route::delete('/kategori/{kategori_buku_id}', 'Kategori_BukuController@destroy');


//ROUTE untuk Buku
Route::resource('buku', 'BukuController');

//Route Profile
Route::resource('profile', 'ProfileController')->only(
    [
        "index", "show"
    ]
);
});

Route::get('/', function () {
    return view('kategori.create');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
